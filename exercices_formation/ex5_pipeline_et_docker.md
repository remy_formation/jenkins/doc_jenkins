# Exercice 5: Pipelines et docker

L'exercice consiste à builder, tester et déployer une image docker via les pipelines déclaratives de Jenkins. Pour des raisons de simplicité, tout va tourner sur le master directement. Dans un scénario idéal, le master distribuerait les étapes sur des agents spécifiques aux environnements : développement, validation, prod... qui auraient chacun des serveurs cibles spécifiques.

But de l'exercice :

- Se familiariser avec les pipelines déclaratives
- Simuler une pipeline à plusieurs étape : build, test, déploiement...

Etapes: 
- Initialisation de la pipeline
- Build d'une image
- On rajoute un test
- Simulation d'une mise en prod

temps estimé : 40 min

## Initialisation de la pipeline

**Objectif : Avoir le squelette de la pipeline pour la suite de l'exercice**

=> Créez un nouveau répertoire GIT. 

Le but de celui-ci sera de contenir le nécessaire pour faire des interactions avec docker. Dans un premier temps, on va déjà initialiser une pipeline

=> Créez un fichier "Jenkinsfile" avec le corps suivant:

```bash
pipeline {
    agent any

    stages {
        stage('init') {
            steps {
                sh 'echo "hello-world"'
            }
        }
    }
}
```

=> Rendez vous sur Jenkins, créez un nouveau répertoire pour l'exercice, puis un nouveau job du type "pipeline". On ne va pas s'intéresser aux options générales ou aux triggers qui sont similaires à ceux d'un job. 

=> Dans la pipeline, choisir d'importer la pipeline depuis un script de SCM et renseignez les informations nécessaires. 

=> Vous pouvez lancer un build pour valider la configuration 

**Etat des lieux:**

- **Vous avez maintenant une pipeline prête**

## **Build d'une image**

**Objectif : Créer votre propre image qui sera utilisé pour les tests et le déploiement**

=> Assurez vous que votre pipeline s'exécute sur votre agent docker

=> Testez qu'on accède bien à docker avec la commande suivante dans le premier steps : 

```bash
# Note : dans cet exercice, toutes les commandes nécessaires vous seront fournies via ce type d'encadré.
# Il n'y a normalement pas d'options à rajouter, juste parfois des éléments à substituer (numéro de port, hostname...)
# Toolbox

docker run hello-world
```

=> Dans le répertoire git, ajoutez un fichier "Dockerfile" :

```Docke
# This file is a template, and might need editing before it works on your project.
FROM httpd:2.4-alpine

RUN echo "This is my super app" > /usr/local/apache2/htdocs/index.html
```

=> Renommez l'étape en "Docker build" et effectuez le build de l'image avec la commande suivant:

```bash
# Note : Docker build va lire et interpréter votre fichier Dockerfile pour créer une image "my_server" en version latest
docker build -t my_server:latest - < Dockerfile
```

=> Rendez vous sur un terminal et faites un :

```bash
docker images
```

Vous devriez voir votre nouvelle image dans la liste.

**Etat des lieux:**

- **L'image my_server est maintenant disponible sur le registre docker local**
- **Note : dans un monde idéal, l'image est à déposer sur un registre à distance**

## On rajoute un test

**Objectif: Rajouter une composante de test à la pipeline**

=> Rajoutez un stage supplémentaire "Test image"

=> Dans les steps :

- instanciez un conteneur
- exécutez le test
- supprimez le conteneur

L'idéal est que la supression du conteneur se fasse systématiquement pour éviter d'avoir un conteneur qui traine.

```bash
# Toolbox
# Note : pour vous simplifier la vie, n'oubliez pas que vous pouvez créer des variables d'environnement dans les pipelines...

# Pour information, l'image qu'on utilise expose le port 80, par contre, vous avez déjà Jenkins sur le port 8080 de la machine
# Vous pouvez utiliser un autre comme 8090
docker run -p <port_host>:<port_conteneur> -d --name <nom_du_conteneur> image:tag

# Vérifier que l'index.html est à jour
[ "This is my super app" = "$(curl http://localhost:<port>/)" ]

# Supprimer un conteneur
docker rm -f test_jenkins


```

**Etat des lieux:**

- **Vous avez un stage de test, avec un clean en post action**

## Simulation d'une mise en prod

**Objectif: Déployer notre nouvelle image**

=> Préparez un fichier docker-compose.yml dans le répertoire git

```bash
# Note : le docker compose est un outil d'IaC (Infra as Code) qui va interpréter votre fichier pour faire la ligne de commande docker équivalente.
web_server:
  image: my_server:latest
  container_name: my_server
  restart: always
  ports:
    - <port_serveur>:<port_conteneur>
```

=> Rajoutez un stage supplémentaire "MEP"

Note : Pour des raisons de simplicité, on va déployer notre "prod" sur le même environnement. C'est bien évidemment à éviter. Un scénario plus vraisemblable est à base de connexion SSH sur un runner spécifique vers des serveurs de prod.

=> Mettez en ligne votre serveur 

```bash
# -d pour rendre la main
# force recreate pour 
docker-compose up -d --force-recreate
```

Attention à une collision des ports entre les tests et la prod !

**Etat des lieux:**

- **Vous pouvez accéder à votre serveur web depuis votre navigateur**









