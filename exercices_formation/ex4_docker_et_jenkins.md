# Exercice 4 : Docker et Jenkins

Sur vos environnements de TP, docker est installé. Seul l'utilisateur "ubuntu" possède les droits sur docker (sans sudo en tout cas...). Dans un contexte d'utilisation normal, on pourrait rajouter les droits à l'utilisateur Jenkins. Cependant, nous ne le ferons pas pour le bien de cet exercice. L'objectif sera plutôt de déployer un node sur votre environnement de TP, qui utilisera "ubuntu" en tant qu'utilisateur, et de privilégier ce nouveau nodes pour vos projects utilisant Docker.

But de l'exercice :

- Se familiariser avec la création d'un node et la notion de labels
- Tester les possibilités de contrôle d'environnement que Docker offre

Etapes: 

- Redéployer un node 
- Vérifier que le node docker fonctionne bien
- Utilisation de docker dans une pipeline

temps estimé : 20 min

## Redéployer un node 

**Objectif : Avoir un node Jenkins capable d'exécuter des commandes docker**

=> Se rendre dans l'administration de Jenkins, dans la partie "Nodes" pour ajouter un nouveau Noeud

=> En nom vous pouvez mettre "docker_node" et en type "Permanent agent"

=> En champs importants vous avez : 

- Répertoire de travail du système distant : Vous pouvez mettre /tmp/node_jenkins pour éviter de polluer votre arborescence avec les fichier de Jenkins

- En étiquettes, Mettez "docker" : ça vous permettre de cibler ce node plus tard

- Utilisation : soit vous permettez à tout le monde de l'utiliser, soit uniquement ceux qui font appel directement à ce node. Sélectionnez "Réserver ce noeud uniquement pour les jobs qui lui sont attachés"

- Méthode de lancement :
  	- Il faut privilégier "Manually trusted key Verification Strategy" et cocher "require manual verification of initial connection" de cette manière vous aurez une option à cliquer au lancement de votre node pour le valider.
  	- Pour le credentials, ajoutez en un avec pour user : ubuntu et pour clé celle que vous avez reçu au début de la formation.
  	- Pour le hostname : mettez localhost. 

  ![prepa_4](..\img\prepa_4.png)

  - Note : Vous remarquerez qu'il y a aussi l'option d'ajouter des variables d'environnements. Nous ne l'utiliserons pas, mais vous pourriez en avoir besoins pour de nombreux usecase réels : Url d'un serveur à partager à tout le monde, Path de différentes versions d'un outil...

=> Une fois enregistré, rendez vous sur votre node, vous devriez avoir une option "Trust SSH Host key" pour finaliser l'installation. Vous pouvez vous rendre dans "journal" pour constater l'installation de l'agent.

**Etat des lieux:**

- **Vous avez maintenant un agent prêt à utiliser Docker**

## Vérifier que le node docker fonctionne bien

**Objectif : Créer un project freestyle pour utiliser docker**

=> Dans l'arborescence projet/docker, créez un projet "freestyle" "test_docker" va valider la disponibilité de Docker en exécutant : 

```shell
docker run hello-world
```

=> Votre job devrait normalement être en erreur : Il faut rajouter l'option pour restreindre son exécution sur les agents type docker.

**Etat des lieux:**

- **Le nouveau node a bien accès à Docker**

## Utilisation de docker dans une pipeline

**Objectif: Rajouter une composante de test à la pipeline**

=> Vous pouvez maintenant utiliser docker dans vos pipelines. Pour tester ça je vous invite à construire une pipeline qui exécutera un "mvn --version" depuis l'image "maven:3.6.3-openjdk-17"

Notes : toutes les informations sur la syntaxe sont ici : https://www.jenkins.io/doc/book/pipeline/syntax/

**Etat des lieux:**

- **Vous êtes capable de travailler avec n'importe quel outil sans avoir à les maintenir sur votre node!**









