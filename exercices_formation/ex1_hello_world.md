# Exercice 1 : Hello world et check d'environnement

Le premier exercice est un moyen de faire une revue des quelques concepts clés et menu de Jenkins nécessaire pour les exercices suivants.

But de l'exercice:

- Créer une arborescence sur Jenkins
- Créer une nouvelle vue
- Créer des jobs d'hello worlds

Etapes clés:

- Création du répertoire
- Créer le job Hello world
- Vérifier si Maven est installé
- Création d'une vue

Temps estimé : 20 min

## Création du répertoire

**Objectif: Créer un répertoire sur Jenkins**

=> A partir du menu, créez un nouvel élément "New item"

=> Choisissez "Folder". Nommer le "tp_hello_world".

=> Dans les configuration, vous pouvez ajouter une "Health metrics". C'est un moyen d'afficher un état de santé comme sur les jobs, mais pour tout le répertoire. Prenez l'option par défaut (child item, recursive)

Note : Pour le pipeline Libraires, on ne met rien. c'est un moyen de partager des bouts de code à utiliser pour tous les projects et pipelines du répertoire. Utile par exemple si vous avez une couche de configuration spécifique à votre répertoire à faire.

**Etat des lieux:**

- **Vous avez maintenant un répertoire pour créer votre job**

## Créer le job Hello world

**Objectif : Initialisation du job permettant de faire le hello-world**

=> Rendez vous dans l'arborescence de votre répertoire nouvellement crée. Faite la création d'un nouvel objet du type "Freestyle Project". Nommez le "test_hello_world"

=> Dans un premier temps, allez directement dans la partie "Build" et ajoutez une étape de type "Execute shell" avec la commande suivante :

```bash
echo "hello-world"
```

=> Lancer un build du job, et vérifiez que dans le logs, la commande s'exécute bien

**Etat des lieux:**

- **On a un premier job de test**

## Vérifier si Maven et python sont installés

**Objectif : Créer un second job pour vérifier si maven est installé**

=> Dans le même répertoire, créez un second job "test_env_setup".

=> Dans la partie build, vérifier ce qui est installé avec la commande:

```bash
mvn --version
python3 --version
```
=> Dans les logs vous devriez retrouver la version de maven et python

**Etat des lieux:**

- **On a un second job de test**

## Création d'une vue

**Objectif : Avoir une vue pour centraliser les tests**

Les vues sont une manière de centraliser des jobs sur Jenkins.

=> Depuis le menu principale de Jenkins, rajoutez une vue avec le "+" à coté de "All"

=> Nommez là "test", avec le type List View

=> Au niveau du filtre des jobs, cochez l'option récursive. Utilisez la regex suivante pour identifier tous les jobs qui contiennent des tests:

```bash
.*test.*
```

=> Validez la vue, vous devriez voir vos deux jobs de test.

**Etat des lieux:**

- **On a une vue qui centralise les tests de tout les répertoires**



