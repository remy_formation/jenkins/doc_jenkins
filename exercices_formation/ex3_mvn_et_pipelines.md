# Exercice 3 : Pipelines et Maven

Dans cet exercice, vous allez récupérer un répertoire git contenant du code Java et utiliser Jenkins pour exécuter les tests unitaires. C'est l'occasion de parcourir quelques fonctionnalités : Code source git, crédenciales, plugins...

But de l'exercice:

- Récupérer des sources GIT
- Utiliser une option de base d'un job : secrets.
- Voir les avantages qu'un plugin peut apporter

Etapes:

- Initialisation du répertoire
- Exécution basique des tests Maven
- Utiliser une pipeline
- Passer à un répertoire git privé
- Build triggers
- Et maintenant avec un plugin

Temps estimé: 45min

## Initialisation du répertoire

**Objectif: Avoir un répertoire git avec lequel interagir** 

=> Rendez vous sur le projet (selon si vous utilisez Gitlab ou github) : 

Gitlab : https://gitlab.com/remy_formation/jenkins/projet_maven

Github : https://github.com/Rymev/exemple_maven

- Faites un fork du projet pour le déposer dans un de vos répertoire. Laissez le "public" pour le moment.

![ex2_fork](..\img\ex2_fork.png)

Note : Sur Github, vous retrouverez le même icône en haut à droite du projet.

:warning: Si vous avez un soucis sur ces opérations, contactez le formateur.

**Etat des lieux:**

- **Vous avez maintenant un répertoire publique avec lequel travailler**

## Exécution basique des tests Maven

**Objectif : Créer les bases du job, avec le minimum d'options pour le moment**

=> Créez un nouveau répertoire /projets/maven 

=> Créez un nouveau project "test_maven". Le but de celui-ci est d'exécuter les tests unitaires qui se trouvent dans le répertoire git. 

=> Dans la partie source Code Management, indiquez une source Git

=> Un menu devrait apparaitre. Indiquer l'URL de votre répertoire (dans mon exemple : https://gitlab.com/remy_formation/jenkins/projet_maven.git). 

Pour les credentials : pas besoins de les préciser pour le moment comme le répertoire est publique.

Attention à la configuration de la branche : depuis quelques années le standard GIT est passé de master à main pour la branche principale. Jenkins est resté sur master pendant que d'autres outils ont mis à jour la valeur par défaut avec main.

=> Pour le build, choisissez l'exécution avec Shell:

```bash
mvn test
```

=> Rendez vous ensuite dans le workspace > target > surefire-reports et vous trouverez les résultat de vos tests

=> A l'instar de ce que vous avez fait pour Python, vous pouvez ajouter l'exposition des résultats de test.

**Etat des lieux:**

- **Le job exécutant les test est maintenant prêt, cependant en cas d'erreur vous ne savez pas quelle partie à disfonctionner : le build ou les tests ?**

## Utiliser une pipeline

**Objectif : avec une pipeline au lieu d'un exécution de job**

=> Retournez dans /projets/maven

=> créez un nouveau project, mais cette fois ci sélectionnez le type "pipeline", nom proposé : test_maven_pipeline

=> Lors de la configuration, vous pouvez soit donner un script directement, soit passer par une pipeline provenant d'un outil de SCM (Source Code Management, autrement dit Git ou SVN). Vous allez devoir utiliser "Pipeline script from SCM". Précisez votre projet, et vous remarquerez que le "Script Path" de base est Jenkinsfile.

=> Si vous exécutez votre test, vous aurez une erreur. Il faut maintenant créer le fameux "Jenkinsfile"

=> Sur votre répertoire gitlab ou github, initialisez un fichier Jenkinsfile. Pour vérifier que ça fonctionne bien, vous pouvez commencer par mettre le contenu de ce fichier "Hello-world":

```bash
pipeline {
    agent any

    stages {
        stage('init') {
            steps {
                sh 'echo "hello-world"'
            }
        }
    }
}
```
=> Une fois que vous avez validé le fonctionnement, vous devez mettre à jour la pipeline avec les consignes suivantes : 
- Deux stages : build et test
- Pour build, un seul step : l'exécution de mvn compile
- Pour test, un seul step : l'éxécution de mvn test

Note : il existe plein d'autres étapes sur maven : install, deploy etc... mais qui nécessite de compléxifier l'environnement dans lequel vous évoluez. Et nous ne sommes pas en formation Maven !

**Etat des lieux:**

- **On commence à se rapprocher d'un vrai cas d'usage. Maintenant, on va sécurisé l'accès à notre répertoire git.**

## Passer à un répertoire git privé

**Objectif: Passer le répertoire en privé et utiliser des crédenciales avec des tokens pour les bonnes pratiques** 

### Sur Gitlab

=> Rendez vous sur votre projet, puis Settings > General > Visibility et passer en "Private"

=> Ensuite générez un token pour la suite (https://gitlab.com/-/profile/personal_access_tokens)

NOTE : :warning: seuls les droits read_repository et write_repository sont nécessaire :warning:

![ex2_token_gitlab](..\img\ex2_token_gitlab.png)

Attention : enregistrez bien le token quelque part, une fois générer vous ne pourrez pas le récupérer à nouveau. Il faudra refaire un token

### Sur Github

=> Rendez vous sur votre projet, puis Settings > General > change visibility et passer le répertoire en privé

Ensuite, générez un token : https://github.com/settings/tokens > Generate new token

les droits "repo" sont suffisants

### Retour sur Jenkins

=> Retournez sur la configuration de votre Job. Dans la partie Source Code Management, il faut maintenant préciser des credentials. Ajoutez un nouveau secret au domaine "Jenkins" pour que d'autres jobs puissent éventuellement en profiter.

> **Pour Github** 

Ne touchez pas au domain et au type (username/password)
Précisez en username votre compte github
En password le token générer avant
En ID un nom unique
En description un petit texte qui sera affiché a coté, pratique pour identifier le secret

> **Pour Gitlab**

Ne touchez pas au domain et au type (username/password)
Précisez en username oauth2
En password le token générer avant
En ID un nom unique
En description un petit texte qui sera affiché a coté, pratique pour identifier le secret

**Etat des lieux:**

- **Le job continue de fonctionner, mais avec un répertoire privé maintenant**

## Build triggers

**Objectif: Lancer un job tout les X temps si il y a eu des changements**

=> Dans les paramètres du job, dans la section "Build Triggers", rajoutez l'option "Scrutation de l'outil de gestion de version" (Poll SCM en anglais). Mettez une période comme toutes les minutes et faite vos tests : faites une modification mineur et attendez de voir le job s'enclencher une seule fois. 

A noter que des méthodes plus intelligentes existent, mais sont liées à la techno du répertoire git (github vs gitlab).

**Etat des lieux:**

- **Le job s'exécute toutes les minutes si le répertoire git à évolué**

## Et maintenant avec un plugin

**Objectif: Découvrir les avantages que peut avoir un plugin**

=> Allez dans le panneau d'administration des plugins et cherchez après "maven integration" et installez le

=> Pour le plugin, il faut configurer maven dans le configuration des outils du panneau d'administration. A la fin, il y a la configuration de Maven

=> Ajouter un maven, nommez le comme vous voulez (ex: mvn3.8.5), décochez l'installation automatique (le produit étant déjà installé...) et préciser le HOME : /usr/share/maven

=> Retournez dans /projets/maven créez un nouveau projet : vous devriez maintenant avoir un nouveau type de projet : "maven project", nom proposé : test_maven_plugin

=> Au niveau de la configuration : renseignez le code source, pas de build triggers (uniquement déclenchement manuel du coups). Par contre pour le build, renseignez le goal : test

=> Si vous retournez sur le projet, vous devriez maintenant avoir le tableau de suivie des résultats des tests. Et si vous allez sur la console, elle est pleinement prise en charge contrairement aux autres types de jobs.

**Etat des lieux:**

- **Le job possède maintenant un graphique qui suivra l'évolution des tests unitaires**





