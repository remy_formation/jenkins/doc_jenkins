# Exercice 2 : Tests unitaires Python et Git

Dans cet exercice, vous allez récupérer un répertoire git contenant du code Python et utiliser Jenkins pour exécuter les tests unitaires. 

But de l'exercice:

- Récupérer des sources GIT
- Utiliser une option de base d'un job : scheduler.

Etapes:

- Exécution basique des tests Python
- Ajouter l'intégration des tests unitaires dans Jenkins
- Scheduler

Temps estimé: 15 min

## Exécution basique des tests Python

**Objectif : Créer les bases du job, avec le minimum d'options pour le moment**

=> Créez un nouveau répertoire  : /projets/python

=> Créez un nouveau project "test_u_python". Le but de celui-ci est d'exécuter les tests unitaires qui se trouvent dans le répertoire git. 

=> Dans la partie source Code Management, indiquez une source Git

=> Un menu devrait apparaitre. Indiquer l'URL de votre répertoire (dans mon exemple :https://gitlab.com/remy_formation/python/test_u.git). 

Pour les credentials : pas besoins de les préciser pour le moment comme le répertoire est publique.

Attention à la configuration de la branche : depuis quelques années le standard GIT est passé de master à main pour la branche principale. Jenkins est resté sur master pendant que d'autres outils ont mis à jour la valeur par défaut avec main.

=> Pour le build, choisissez l'exécution avec Shell:

```bash
python3 -m pytest --verbose --junitxml result.xml
```

=> Rendez vous ensuite dans le workspace > result.xml pour retrouver le résultat de vos tests.

**Etat des lieux:**

- **Le job exécutant les test est maintenant prêt, on s'occupera plus tard de la gestion de l'authentification.**

## Ajouter l'intégration des tests unitaires dans Jenkins

**Objectif : Avoir une vue sur l'évolution des tests facile**

=> Retournez dans la configuration du job.

=> Dans la section "Actions à la suite du build", vous retrouverez l'option "publier le rapport de résultats des tests Junit"

=> Vous avez simplement à indiquer l'emplacement de votre rapport.

=> Relancez votre job, après actualisation devriez avoir un rapport sur la droite de la page de votre job.

**Etat des lieux:**

- **Le job exécutant les test est maintenant prêt, la gestion de l'authentification ainsi que l'exploitation des résultats se faire plus tard**

## Scheduler

**Objectif: Aborder les scheduler. Les options de Jenkins de base ne sont pas exceptionnels mais suffisante pour nos TP**

=> Dans les paramètres du job, dans la section "Build Triggers", rajoutez l'option "Build periodically". Vous pouvez commencer par tester avec un build toutes les minutes (syntaxe cron: * * * * *), puis passer à 5 minutes.

Site utile : https://crontab.guru/

Note : on pourra tester une méthode qui se base sur l'update du code plus tard : Poll SCM

A noter que des méthodes plus intelligentes existent, comme les hooks, mais sont liées à la techno du répertoire git (github vs gitlab).

**Etat des lieux:**

- **Le job s'exécute maintenant toutes les 5 minutes.**





