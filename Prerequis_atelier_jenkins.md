
# Prérequis pour les ateliers Jenkins

- Une VM pour héberger Jenkins (sous docker)
- Un compte gitlab.com (https://gitlab.com/users/sign_in)


## Création de la VM

Créer une VM linux ubuntu x64 avec pour image, ubuntu 20.04 (la LTS)

En capacité :

RAM  : 4Go  (au minimum)

Stockage : 30 Go ( 50Go Idéalement)

Cpu : au mieux

**Faire une installation minimale pour limiter l'usage disque**

## Autoriser les copié / collé :

- une fois la VM en cours exécution: Périphérique > Installer l’image CD des additions invités

- Redémarrer, puis une fois la VM en cours exécution : Périphérique >  Presse-papier partagé, choisir bidirectionnel




## Installation de docker

```bash
 # Prérequis
 sudo apt-get update
 sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
    
# Docker GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Ajout des repo docker
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    
# Installatin de docker:
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
 
# On ajoute docker à l'utilisateur courant
sudo groupadd docker
sudo usermod -aG docker $USER

# On reboot et c'est bon normalement, vous pouvez tester avec:
docker run hello-world
```

## Installer Jenkins

```bash
# Créer le compte Jenkins
sudo adduser jenkins
# Donner les droits de docker a Jenkins
sudo usermod -aG docker jenkins

# Installer docker-compose
sudo apt-get install docker-compose -y
```

Ensuite, créez le fichier suivant, nommé le "docker-compose.yml"

```yaml
jenkins_master:
  image: jenkins/jenkins:2.332.3
  container_name: jenkins
  user: root
  restart: always
  volumes:
    # On autorise l'utilisation de dokcer
    #- /var/run/docker.sock:/var/run/docker.sock
    #- /usr/bin/docker:/usr/bin/docker
    - $HOME/jenkins_root:/root
    - $HOME/jenkins:/var/jenkins_home
  ports:
    - 8080:8080
```

Et instanciez votre Jenkins

```bash
# -d pour le faire tourner en mode 'deamon"
docker-compose up -d

# récupérez le mdp par défaut
sudo cat jenkins/secrets/initialAdminPassword
```

Ensuite, rendez vous sur http://localhost:8080/

- rentrez le mot de passe obtenu avant
- choisissez "Install sugested plugins" (l'installation prendra quelques minutes)
- Initialisez un utilisateur (je vous conseil admin/admin pour le TP)
- Laissez http://localhost:8080/ pour le domaine

Et vous devriez arriver normalement sur la page principale de Jenkins.

## Installation  et configuration SSH

La VM servira de "slave" au conteneur Jenkins, on va donc ajouté la possibilité de faire du ssh :

```bash
sudo apt-get install openssh-server -y
```

Vous pouvez tester la connexion :

```bash
docker exec -ti jenkins /bin/bash

# Puis, à l'interieur du conteneur
ssh jenkins@172.17.0.1
```

## Installer Maven

Pour la VM et les ateliers, on aura besoins de java et maven

```bash
# Installer java
sudo apt install openjdk-17-jre-headless

# Installer Maven
wget https://dlcdn.apache.org/maven/maven-3/3.8.5/binaries/apache-maven-3.8.5-bin.tar.gz -P /tmp
sudo tar xf /tmp/apache-maven-3.8.5-bin.tar.gz -C /opt
# Lien symbolique
sudo ln -s /opt/apache-maven-3.8.5 /opt/maven

# tester mvn:
export M2_HOME=/opt/maven
export PATH=${M2_HOME}/bin:${PATH}
mvn --version

```

## Configuration du slave

Pour terminer, on va configurer un slave pour les TP

On va sur Manage Jenkins > Manage Nodes and clouds

![prepa_1](img\prepa_1.png)

New node

Node name: jenkins_tp

type : cochez "Permanent Agent"

![prepa_2](img\prepa_2.png)



Ensuite, on remplie les informations :

![prepa_3](img\prepa_3.png)

Pour le Credentials, les informations se trouvent sur l'image suivante

![prepa_4](img\prepa_4.png)

:warning: Le mot de passe / username correspond à votre compte Jenkins sur la VM

![prepa_5](C:\Users\Rémy\Desktop\Extia\formation\Jenkins\doc_jenkins\img\prepa_5.png)

Cliquez sur Add, puis il reste à valider le fingerprint manuellement :

![prepa_6](img\prepa_6.png)

![prepa_7](img\prepa_7.png)

![prepa_8](img\prepa_8.png)

Et ensuite il faut valider le fingerprint pour que le runner soit prêt.