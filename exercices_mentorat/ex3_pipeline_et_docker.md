# Exercice 3 : Pipelines et docker

:warning: Faire une relcture de la partie docker pour la simplifier

L'exercice trois consisté à builder, tester et déployer une image docker via les pipelines déclaratives de Jenkins. Pour des raisons de simplicité, tout va tourner sur le slave directement. Dans un scénario idéal, le slave exécuterait les commandes sur des serveurs de validation et de prod.

But de l'exercice :

- Se familiariser avec les pipelines déclaratives
- Simuler une pipeline à plusieurs étape : build, test, déploiement...

Etapes: 
- Initialisation de la pipeline
- Build d'une image
- On rajoute un test
- Simulation d'une mise en prod

temps estimé : 30min

## Initialisation de la pipeline

**Objectif : Avoir le squelette de la pipeline pour la suite de l'exercice**

=> Créez un nouveau répertoire GIT. 

Le but de celui-ci sera de contenir le nécessaire pour faire des interactions avec docker. Dans un premier temps, on va déjà initialiser une pipeline

=> Créez un fichier "Jenkinsfile" avec le corps suivant:

```bash
pipeline {
    agent any

    stages {
        stage('init') {
            steps {
                sh 'echo "hello-world"'
            }
        }
    }
}
```

=> Rendez vous sur Jenkins, créez un nouveau répertoire pour l'exercice, puis un nouveau job du type "pipeline". On ne va pas s'intéresser aux options générales ou aux triggers qui sont similaires à ceux d'un job. 

=> Dans la pipeline, choisir d'importer la pipeline depuis un script de SCM et renseignez les informations nécessaires. 

=> Vous pouvez lancer un build pour valider la configuration 

**Etat des lieux:**

- **Vous avez maintenant une pipeline prête**

## **Build d'une image**

**Objectif : Créer votre propre image qui sera utilisé pour les tests et le déploiement**

=> Sécurisez l'agent en changeant le "any" par { label 'docker' }

Note : ici l'agent est défini au niveau global, mais on pourrait le définir stage par stage si nécessaire.

=> Testez qu'on accède bien à docker avec la commande suivante dans le premier steps : 

```bash
docker run hello-world
```

=> Dans le répertoire git, ajoutez un fichier "Dockerfile" :

```Docke
# This file is a template, and might need editing before it works on your project.
FROM httpd:2.4-alpine

RUN echo "This is my super app" > /usr/local/apache2/htdocs/index.html
```

=> Renommez l'étape en "Docker build" et effectuez le build de l'image avec la commande suivant:

```bash
docker build -t my_server:latest - < Dockerfile
```

=> Rendez vous sur un terminal de votre VM (qui est votre slave Jenkins) et faites un :

```bash
docker images
```

Vous devriez voir votre nouvelle image dans la liste.

**Etat des lieux:**

- **L'image my_server est maintenant disponible sur le registre docker local**
- **Note : dans un monde idéal, l'image est à déposer sur un registre à distance**

## On rajoute un test

**Objectif: Rajouter une composante de test à la pipeline**

=> Rajoutez un stage supplémentaire "Test image"

=> Dans les steps, instanciez un conteneur, exécuter le test et le supprimer le conteneur

L'idéal est que la supression du conteneur se fasse systématiquement pour éviter d'avoir un conteneur qui traine.

```bash
# Toolbox

# Pour information, l'image qu'on utilise expose le port 80
docker run -p <port_host>:<port_conteneur> -d --name <nom_du_conteneur> image:tag

# Vérifier que l'index.html est à jour
[ "This is my super app" = "$(curl http://localhost:<port>/)" ]

# Supprimer un conteneur
docker rm -f test_jenkins


```

**Etat des lieux:**

- **Vous avez un stage de test, avec un clean en post action**

## Simulation d'une mise en prod

**Objectif: Déployer notre nouvelle image**

=> Préparez un fichier docker-compose.yml dans le répertoire git

```bash
web_server:
  image: my_server:latest
  container_name: my_server
  restart: always
  ports:
    - <port_serveur>:<port_conteneur>
```

=> Rajoutez un stage supplémentaire "MEP"

Pour des raisons de simplicité, on va déployer notre "prod" sur le slave directement. C'est bien évidemment à éviter. On peut imaginer un scénario plus complexe à base de connexion SSH sur un runner spécifique vers des serveurs de prod.

=> Mettez en ligne votre serveur 

```bash
# -d pour rendre la main
# force recreate pour 
docker-compose up -d --force-recreate
```

Attention à une collision des ports entre les tests et la prod !

**Etat des lieux:**

- **Vous pouvez accéder à votre serveur web depuis votre navigateur**









