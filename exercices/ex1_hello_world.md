# Exercice 1 : Hello world et quelques actions basiques

Le premier exercice est un moyen de faire une revue des quelques concepts clés et menu de Jenkins nécessaire pour les exercices suivants.

But de l'exercice:

- Créer une arborescence sur Jenkins
- Créer une nouvelle vue
- Créer des jobs d'hello worlds
- Faire quelques opérations d'administration
- Ajouter un plugin Jenkins

Etapes clés:

- Création du répertoire
- Créer le job Hello world
- Vérifier si Maven est installé
- Création d'une vue
- Opération de maintenance et labels
- Ajout d'un plugin

Temps estimé : 45 min

## Création du répertoire

**Objectif: Créer un répertoire sur Jenkins**

=> A partir du menu, créez un nouvel élément "New item"

=> Choisissez "Folder". Nommer le "tp_hello_world".

=> Dans les configuration, vous pouvez ajouter une "Health metrics". C'est un moyen d'afficher un état de santé comme sur les jobs, mais pour tout le répertoire. Prenez l'option par défaut (child item, recursive)

Note : Pour le pipeline Libraires, on ne met rien. c'est un moyen de partager des bouts de code à utiliser pour tous les projects et pipelines du répertoire.

**Etat des lieux:**

- **Vous avez maintenant un répertoire pour créer votre job**

## Créer le job Hello world

**Objectif : Initialisation du job permettant de faire le hello-world**

=> Rendez vous dans l'arborescence de votre répertoire nouvellement crée. Faite la création d'un nouvel objet du type "Freestyle Project". Nommez le "test_hello-world"

=> Dans un premier temps, allez directement dans la partie "Build" et ajoutez une étape de type "Execute shell" avec la commande suivante :

```bash
echo "hello-world"
```

=> Lancer un build du job, et vérifiez que dans le logs, la commande s'exécute bien

**Etat des lieux:**

- **On a un premier job de test**

## Vérifier si Maven est installé

**Objectif : Créer un second job pour vérifier si maven est installé**

=> Dans le même répertoire, créez un second job "test_maven_exist".

=> Dans la partie build, vérifier que Maven est installé avec la commande:

```bash
export M2_HOME=/opt/maven
export PATH=${M2_HOME}/bin:${PATH}
mvn --version
```
Note : faire l'ajout des variables d'environnement est pénible. On s'en débarrassera dans l'exercice suivant.

=> Dans les logs vous devriez retrouver la version de maven 3.8.5

**Etat des lieux:**

- **On a un second job de test**

## Création d'une vue

**Objectif : Avoir une vue pour centraliser les tests**

Les vues sont une manière de centraliser des jobs sur Jenkins.

=> Depuis le menu principale de Jenkins, rajoutez une vue avec le "+" à coté de "All"

=> Nommez là "test", avec le type List View

=> Au niveau du filtre des jobs, cochez l'option récursive. Utilisez la regex suivante pour identifier tous les jobs qui contiennent des tests:

```bash
.*test.*
```

=> Validez la vue, vous devriez voir vos deux jobs de test.

**Etat des lieux:**

- **On a une vue qui centralise les tests de tout les répertoires**

## Opération de maintenance et labels

**Objectif : Simuler une opération de maintenance pour voir l'importance des labels**

=> Allez dans la partie administration de Jenkins

=> Dans la gestion des nœuds, rendez vous sur le nœud "slave" jenkins_tp

=> En haut à droite, vous devriez pouvoir passer le nœud temporairement hors ligne

=> Une fois fait, rendez vous sur votre job de test de Maven et tentez de l'exécuter à nouveau

Vous devriez avoir une erreur : le slave étant hors ligne, vous exécutez le job sur le master qui ne possède pas l'outil. 

=> Modifiez votre job pour s'exécuter uniquement sur les nœuds de type "mvn" et relancez un build

Vous devriez maintenant avoir un build en attente d'un slave.

=> Réactivez votre nœud et constatez que le job c'est bien exécutée.

**Etat des lieux**:

- **Votre job Jenkins pour vérifier si Maven est présent s'exécutera maintenant uniquement sur les nodes type "mvn"**

## Ajout d'un plugin

**Objectif: rajouter un plugin à Jenkins**

=> Rendez vous dans la partie gestion de Jenkins

=> Rendez vous dans la partie gestion des plugins

=> Dans la section des plugins disponibles, cherchez après "Chuck Norris"

=> Cochez la case et faite l'installation

=> Rendez vous maintenant sur un de vos jobs. En fin de configuration, dans les actions post-job, vous pouvez ajouter l'option "activer chuck norris"

=> Si vous voulez tester un scénario "FAIL", vous pouvez rajouter un "exit 1" (n'importe quel code autres que 0) pour sortir un build en échec.

=> Revenez sur la console d'administration, si vous avez des erreurs (rouges), elles correspondent à des dépendances installées qui ne sont pas à jour. Rendez vous sur le panneau de gestion des plugins. Dans la partie updates, en bas vous avez une option pour sélectionner tous les plugins. Sélectionnez les tous les plugins et faite une mise à jour. Les erreurs devraient disparaitre.  

**Etat des lieux:**

- **Vous avez ajouté une interaction supplémentaire à Jenkins via un plugin**



