# Exercice 2 : Intégration d'un répertoire git

Dans cet exercice, vous allez récupérer un répertoire git contenant du code Java et utiliser Jenkins pour exécuter les tests unitaires. C'est l'occasion de parcourir quelques fonctionnalités : Code source git, crédenciales, plugins...

But de l'exercice:

- Récupérer des sources GIT
- Utiliser les options de base d'un job : secrets, scheduler...
- Gérer les variables d'environnements via les nodes
- Voir les avantages qu'un plugin peut apporter

Etapes:

- Initialisation du répertoire
- Exécution basique des tests Maven
- Passer à un répertoire git privé
- Variables d'environnement
- Scheduler
- Et maintenant avec un plugin

Temps estimé: 45min

## Initialisation du répertoire

**Objectif: Avoir un répertoire git avec lequel interagir** 

=> Rendez vous sur le projet : https://gitlab.com/remy_formation/jenkins/projet_maven

- **Si vous avez un compte gitlab.com**, faites un fork du projet pour le déposer dans un de vos répertoire. Laissez le "public" pour le moment.

![ex2_fork](..\img\ex2_fork.png)

- **Si vous avez un compte github.com**, faites un projet vide, laissez le "public" pour le moment, puis utilisez l'option "import code" 

:warning: TODO : Détaillez les deux manipulations avec des screens. Si vous avez un soucis sur ces opérations, contactez le formateur 

**Etat des lieux:**

- **Vous avez maintenant un répertoire publique avec lequel travailler**

## Exécution basique des tests Maven

**Objectif : Créer les bases du job, avec le minimum d'options pour le moment**

=> Créez un nouveau répertoire (exercice_2)

=> Créez un nouveau project "maven_test". Le but de celui-ci est d'exécuter les tests unitaires qui se trouvent dans le répertoire git. 

=> Dans la partie générale, restreignez le projet aux nodes maven (mvn)

=> Dans la partie source Code Management, indiquez une source Git

=> Un menu devrait apparaitre. Indiquer l'URL de votre répertoire (dans mon exemple : https://gitlab.com/remy_formation/jenkins/projet_maven.git). 

Pour les credentials : pas besoins de les préciser pour le moment comme le répertoire est publique.

Attention à la configuration de la branche : depuis quelques années le standard GIT est passé de master à main pour la branche principale. Jenkins est resté sur master pendant que d'autres outils ont mis à jour la valeur par défaut avec main.

=> Pour le build, choisissez l'exécution avec Shell:

```bash
export M2_HOME=/opt/maven
export PATH=$M2_HOME/bin:$PATH
mvn test
```

=> Rendez vous ensuite dans le workspace > surefire-reports et vous trouverez les résultat de vos tests

**Etat des lieux:**

- **Le job exécutant les test est maintenant prêt, mais quelques détails sont à corriger**

## Passer à un répertoire git privé

**Objectif: Passer le répertoire en privé et utiliser des crédenciales avec des tokens pour les bonnes pratiques** 

=>**Sur Gitlab**, rendez vous sur votre projet, puis Settings > General > Visibility et passer en "Private"

Ensuite générez un token pour la suite (https://gitlab.com/-/profile/personal_access_tokens)

![ex2_token_gitlab](..\img\ex2_token_gitlab.png)

Attention : enregistrez bien le token quelque part, une fois générer vous ne pourrez pas le récupérer à nouveau. Il faudra refaire un token

=> **Sur Github**, rendez vous sur votre projet, puis Settings > General > change visibility et passer le répertoire ne privé

Ensuite, générez un token : https://github.com/settings/tokens > Generate new token

les droits "repo" sont suffisants

=> Retournez sur la configuration de votre Job. Dans la partie Source Code Management, il faut maintenant préciser des credentials. Ajoutez des nouveaux secrets au domaine "Jenkins" pour que d'autres jobs puissent éventuellement en profiter.

> **Pour Github** 

Ne touchez pas au domain et au type (username/password)
Précisez en username votre compte github
En password le token générer avant
En ID un nom unique
En description un petit texte qui sera affiché a coté, pratique pour identifier le secret

> **Pour Gitlab**

Ne touchez pas au domain et au type (username/password)
Précisez en username oauth2
En password le token générer avant
En ID un nom unique
En description un petit texte qui sera affiché a coté, pratique pour identifier le secret

**Etat des lieux:**

- **Le job continue de fonctionner, mais avec un répertoire privé maintenant**

## Variables d'environnement

**Objectif: Simplifions la commande du script en configurant les variables d'environnement en amont**

=> Rendez vous dans le panneau d'administration de Jenkins, puis la gestion des noeuds

=> Rendez vous dans la configuration du noeud de tp

 => Rentrez dans la configuration, dans la partie "Node Properties", rajoutez les variables d'environnements suivantes:

```bash
M2_HOME=/opt/maven
PATH=${M2_HOME}/bin:${PATH}
```

Pour information, on aurait pu essayer de rajouter ces variables dans le bashrc ou le profile du compte Jenkins, mais Jenkins a tendance à effacer les variables d'environnement pour fournir les siennes.

=> Modifiez le job pour supprimer les "export" et vérifiez que le job fonctionne toujours

**Etat des lieux:**

- **La commande du job ne peut pas être plus simple**

## Scheduler

**Objectif: Aborder les scheduler. Les options de Jenkins de base ne sont pas exceptionnels mais suffisante pour nos TP**

=> Dans les paramètres du job, dans la section "Build Triggers", rajoutez l'option "Build periodically". Vous pouvez commencer par tester avec un build toutes les minutes (syntaxe cron: * * * * *)

Site utile : https://crontab.guru/

=> Sinon, vous pouvez tester le Poll SCM : il va vérifier à interval régulier si le code à changez et build en conséquence. N'oubliez pas de faire un changement dans votre répertoire si vous voulez voir un nouveau build !

A noter que des méthodes plus intelligentes existent, mais sont liées à la techno du répertoire git (github vs gitlab)

**Etat des lieux:**

- **Le job s'exécute maintenant toutes les minutes, ou toutes les minutes si le répertoire git à évolué**

## Et maintenant avec un plugin

**Objectif: Découvrir les avantages que peut avoir un plugin**

=> Allez dans le panneau d'administration des plugins et cherchez après "maven integration" et installez le

=> Pour le plugin, il faut configurer maven dans le configuration des outils du panneau d'administration. A la fin, il y a la configuration de Maven

=> Ajouter un maven, nommez le comme vous voulez (ex: mvn3.8.5), décochez l'installation automatique (le produit étant déjà installé...) et préciser le HOME : /opt/maven

=> Dans votre répertoire exercice 2 créez un nouveau projet : vous devriez maintenant avoir un nouveau type de projet : "maven project"

=> Au niveau de la configuration : restreignez l'exécution aux nodes mvn, renseignez le code source, pas de build triggers. Par contre pour le build, renseignez le goal : test

=> Si vous retournez sur le projet, vous devriez maintenant avoir le tableau de suivie des résultats des tests

**Etat des lieux:**

- **Le job possède maintenant un graphique qui suivra l'évolution des tests unitaires**





